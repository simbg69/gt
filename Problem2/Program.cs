﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem2
{
    class Program
    {
        /*
        Problem 2
        $1, 50c, 20c, 10c and 5c are the denominations of coins in common circulation here. 
        Write a function/method/subroutine which takes as its input a representation of an amount of money, 
        and then determines the smallest combination of coins of that will add up to the input amount. 

        The output should be an array/list of numbers, represent the number of coins for each denomination. 
        For example, if the input represents $1.95, the output should be [1, 1, 2, 0, 1], meaning
        Denomination	Number
        $1	1
        50c	1
        20c	2
        10c	0
        5c	1
        Total: $1.95

        (Assume that the input amount will be in multiples of 5 cents.)
        */

        static void Main(string[] args)
        {
            List<int> output = FindSmallestCombination(args[0]);

            for (int i = 0; i < output.Count; i++)
            {
                Console.Write(output[i] + (i+1 < output.Count ? ", " : ""));
            }
        }

        static List<int> FindSmallestCombination(string inputAmount)
        {
            decimal[] coinDenominationsInCirculation = { 1.00m, 0.50m, 0.20m, 0.10m, 0.05m };

            List<int> smallestCombination = new List<int>();

            try
            {
                decimal balanceAmount = Convert.ToDecimal(inputAmount);

                for (int i = 0; i < coinDenominationsInCirculation.Length; i++)
                {
                    smallestCombination.Add(FindNumOfCoinsForDenomination(balanceAmount, coinDenominationsInCirculation[i]));

                    balanceAmount -=
                        smallestCombination[smallestCombination.Count - 1]
                        * coinDenominationsInCirculation[i];
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception occurred:  " + e.ToString());
            }

            return smallestCombination;
        }

        static int FindNumOfCoinsForDenomination(decimal balanceAmount, decimal denominationValue)
        {
            int numOfCoins = 0;

            try
            {
                while ((numOfCoins+1) * denominationValue <= balanceAmount)
                {
                    numOfCoins++;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception occurred:  " + e.ToString());
            }

            return numOfCoins;
        }

    }
}
