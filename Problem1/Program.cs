﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1
{
    /*
    Problem 1
    Given 2 strings – A and B, the first longest substring of A which contains all characters of B is called Maximum Window 
    Task: You need to write a function 'find_maximum_window' which returns maximum window for A and B.
    If there is no such window, then return "no such window".
    Input A	Input B	Output
    There are so many interesting projects in GOVTECH	tes	interesting

    Example, for this problem =>    
    Input Strings: 
    A: "There are so many interesting projects in GOVTECH " 
    B: "tes"
    Output should be: "interesting" since it is the longest sub string having all characters of "test" ie t,e,s
    */

    class Program
    {
        static void Main(string[] args)
        {
            string maximum_window = FindMaximumWindow(args[0], args[1]);

            Console.WriteLine(maximum_window);
        }

        class Window
        {
            public string Substring { get; set; }
            public int IndexOfSubstringWithinParentstring { get; set; }
        }

        //This implementation treats character duplicates in B as single character e.g. tee is processed similarly as te
        static string FindMaximumWindow(string A, string B)
        {
            string maximum_window = "no such window";

            try
            {
                string[] subStringsOfA = A.Split(' ');
                char[] charsOfB = B.ToCharArray();

                List<Window> windows = new List<Window>();

                for (int i = 0; i < subStringsOfA.Length; i++)
                {
                    bool isWindow = true;

                    for (int j = 0; j < charsOfB.Length; j++)
                    {
                        if (subStringsOfA[i].IndexOf(charsOfB[j]) == -1)
                        {
                            isWindow = false;
                            break;
                        }
                    }

                    if (isWindow)
                    {
                        windows.Add(new Window { Substring = subStringsOfA[i], IndexOfSubstringWithinParentstring = i });
                    }
                }

                if (windows.Count > 0)
                {
                    //sort first by length to get the longest windows at the top, then by index in parent string to get the first longest window.
                    var sorted = windows.OrderByDescending(w => w.Substring.Length).ThenBy(w => w.IndexOfSubstringWithinParentstring);

                    maximum_window = sorted.ToList()[0].Substring;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception occurred:  " + e.ToString());

                maximum_window = "no such window";
            }

            return maximum_window;
        }
    }
}
