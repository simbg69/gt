﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem4
{
    class Program
    {
        /*
        Problem 4
        Palindromes are words or phrases that read the same backward and forward, letter for letter, 
        number for number, or word for word 

        A substring is a contiguous subset of characters in a string.

        The intention is to Find distinct palindrome substring of an input string s.

        For example, s = aabaa.
        Its distinct palindromic substrings are [a, aa, aabaa, aba, b].

        Write a function that accept a string s as input parameter and output the following:
        Input	Output
        aabaa	Input String :
         s = aabaa
 
         Length of input string:
         5

         Explanation :
         Palindromic substring are [a, aa, aabaa, aba, b]

        Note:
         The substring "a" occurs 4 times, but is counted only once.  
         Similarly, "aa" occurs twice but counts as one distinct palindrome.
        Constraints:
        1 <= |s| <= 5000
        Each character s[i] belongs ascii[a-z]         
        */
        static void Main(string[] args)
        {
            HashSet<string> distinctPalindromes = FindDistinctPalindromeSubstring(args[0]);

            Console.WriteLine("Input String :");
            Console.WriteLine("s = " + args[0]);

            Console.WriteLine("");

            Console.WriteLine("Length of input string:");
            Console.WriteLine(args[0].Length.ToString());

            Console.WriteLine("");

            Console.WriteLine("Explanation:");
            Console.Write("Palindromic substring are [");
            //for (int i = 0; i < output.Count; i++)
            foreach (string palindrome in distinctPalindromes)
            {
                Console.Write(palindrome + " ");
            }
            Console.Write("]");
        }

        static HashSet<string> FindDistinctPalindromeSubstring(string inputString)
        {
            //List<string> palindromeSubstrings = new List<string>();
            HashSet<string> distinctPalindromeSubstrings = new HashSet<string>();

            int startIndex;
            int length;

            try
            {
                for (startIndex = 0; startIndex < inputString.Length; startIndex++)
                {
                    for (length = 1; length <= inputString.Length - startIndex; length++)
                    {
                        if (IsPalindrome(inputString.Substring(startIndex, length)))
                        {
                            distinctPalindromeSubstrings.Add(inputString.Substring(startIndex, length));
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception occurred:  " + e.ToString());
            }

            return distinctPalindromeSubstrings;
        }

        static bool IsPalindrome(string input)
        {
            for (int i = 0; i < input.Length; i++)
            {
                if (input[i] != input[input.Length - 1 - i])
                {
                    return false;
                }
            }

            return true;
        }
    }
}
