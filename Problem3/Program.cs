﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem3
{
    class Program
    {
        /*
        Write a function/method/subroutine that takes as input an integer ranging from 0-99. 
        It should output the English equivalent of the integer value. E.g.

        Input	Output
        12	Twelve
        73	Seventy three
        30	Thirty

        Ensure that your function/method/subroutine is easily extensible          
        */
        static void Main(string[] args)
        {
            string englishEquivalentOfIntegerValue = IntegerToWords(args[0]);

            Console.WriteLine(englishEquivalentOfIntegerValue);
        }

        //Modified from https://social.msdn.microsoft.com/Forums/vstudio/en-US/0b1dec9c-61e9-4544-8134-bda1264a21a4/how-to-convert-number-into-words-in-c?forum=csharpgeneral
        static string IntegerToWords(string integerValue)
        {
            string words = "";

            try
            {
                int integer = Convert.ToInt32(integerValue);

                var unitsMap = new[] { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
                var tensMap = new[] { "zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

                if (integer < 20)
                    words += unitsMap[integer];
                else
                {
                    words += tensMap[integer / 10];
                    if ((integer % 10) > 0)
                        words += " " + unitsMap[integer % 10];
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception occurred:  " + e.ToString());

                words = "Currently only accept as input an integer ranging from 0-99";
            }

            return words;
        }
    }
}
